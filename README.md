### Project launching
First, you have to download every file!

Please, Follow the next steps:
 
 OrangePI and Sensor:
  - Download orangepi_python directory
  - Install all Python package on the OrangePI (the description in the thesis)
  - Copy the orangepi_python directory to an USB drive and connect it to the OrangePI
  - After the USB mount you can run the dht11_start.py

 Application:
  - Download the apk file from the mobil_app directory
  - Install it on your phone/device
  - After the installation you can run the program
  - Every next steps in the thesis

 Node Server:
  - Download the files from the server directory
  - Open the directory with terminal and run these:
    ```sh
	$ npm install
	$ node app.js
	```

That's all.

Everything what you see has made for a BSc thesis presentation.
