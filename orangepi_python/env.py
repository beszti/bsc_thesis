import os

FLASK_PORT = os.getenv('FLASK_PORT', default=3001)
