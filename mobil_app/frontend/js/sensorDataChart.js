var celCtx = document.getElementById("celsiusCanvas").getContext("2d");

Chart.defaults.global.defaultFontColor = 'orange';

let celsius = [];
let humidity = [];

fetch(serverURL + '/oneweek')
.then(async(resp) => {
	let data = await resp.json()
	celsius = data.celsius;
	humidity = data.humidty;
})

var myLineChart = new Chart(celCtx, {
    type: 'line',
    data:{
    	labels: ['7 napja', '6 napja', '5 napja', '4 napja', '3 napja', '2 napja', '1 napja'],
		datasets: [{
			label: 'hőmérséklet',
			fontColor: 'orange',
			backgroundColor: 'red',
			borderColor: 'red',
			fontColor: 'black',
			data: [
				20,
				21,
				22,
				21,
				22,
				23,
				24
			],
			fill: false,
		}]
    },
    options: {
    	responsive: true,
    	title: {
            display: true,
            text: 'Hőmérséklet kimutatás',
            fontColor: 'orange',
            fontSize: '15',
            fontStyle: 'none'
        },
        layout: {
            padding: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					fontColor: 'orange'
				}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Hőmérséklet [°C]',
					fontColor: 'orange'
				}
			}]
		}

    }
});

var humCtx = document.getElementById("humidityCanvas").getContext("2d");
var myLineChart = new Chart(humCtx, {
    type: 'line',
    data:{
    	labels: ['7 napja', '6 napja', '5 napja', '4 napja', '3 napja', '2 napja', '1 napja'],
		datasets: [{
			label: 'páratartalom',
			fontColor: 'orange',
			fill: false,
			backgroundColor: 'blue',
			borderColor: 'blue',
			data: [
				29,
				33,
				34,
				41,
				36,
				34,
				35
			],
		}]
    },
    options: {
    	responsive: true,
    	labels: {
    		fontColor: 'orange'
    	},
    	title: {
            display: true,
            text: 'Páratartalom kimutatás',
            fontColor: 'orange',
            fontSize: '15',
            fontStyle: 'none'
        },
        layout: {
            padding: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					fontColor: 'orange'
				}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Páratartalom [%]',
					fontColor: 'orange'
				}
			}]
		}

    }
});

//===============================================================
/*
window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};


	var Months = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	];
*/