const express = require('express'); //requiere megmondja h node modult akarok
const app = express(); //express appot tárolja
const mongoose = require('mongoose')

var cors = require('cors') //package, melyben engedélyek vannak, hogy külsö kérés is hozzáférjen az adatokhoz (fetch)

const path = require('path'); //node segédmodul, fájl explorer utvonalakkal segit, bizonos met-ekkel

var router = express.Router(); //
var index = require("./routes/index"); //eléri az indexet

app.use(cors())

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies

mongoose
  .connect(
    'mongodb://localhost:27017/DataFromPI',
    { useNewUrlParser: true }
  )
  .then(() => {
    console.log("Mongodb connected...");
  })
  .catch(err => {
    console.log("Mongodb error... ", err.stack);
    process.exit(1);
  });

app.use(express.json()); //lekérjük a json metudost, aminek segitségével applikácionk már értelmezi a json architekturákat/objektumokat

app.use(express.static(path.join(__dirname, "public"))); //beállitottam a public mappát, a static mondja meg h mit láthat a böngészöböl

app.use(index, router);//1. megmondom, h mi a föindex fájlom|| 2. főrouternek állitom be az indexet

app.listen(3000, () => console.log("mongodb-s programom port:3000"));

module.exports = router;//ez a fájl visszatér az express routerjével