var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');
const fetch = require('node-fetch');
const SMS = require('node-sms-send');
const sms = new SMS('username', 'password');

let orangePiIpAddress = 'http://192.168.100.134:3001';

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
	host: "smtp.gmail.email",
	service: "gmail",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
		user: 'example@gmail.com', // valid email
		pass: '********'              // valid password for email
    }
});

var Schema = mongoose.Schema;

const piSchema = new Schema({
	celsius : Number,
	humidity: Number,
	smoke: Number,
	date: Date,
	pi: Number
});
const DataModel = mongoose.model('data', piSchema, 'dataFromPI');

const userSchema = new Schema({
	name : String,
	email: String,
	password: String,	
	city: String,
	zip: String,
	address: String,
	phone: String
});
const UserModel = mongoose.model('user', userSchema, 'users');

/**
 * Get data in every 2 sec and
 * separate the temperature from the humidity.
 * Send datas to another function that load up to the DB.
 */
let dataEvery2Second;
let celsius = [];
let humidity = [];
let dataFromPi = [];
setInterval(async function readDataFromPI() {
	fetch(orangePiIpAddress + '/datas')
	.then(async(resp) => {
		dataEvery2Second = await resp.json();
		if(dataEvery2Second !== null){
			let resHyst = hysteresis(dataEvery2Second);
			if (resHyst !== 0) {
				// sendEmail(resHyst);
			}

			if (celsius.length < 15) {
				// Separate the C and H
				celsius.push(dataEvery2Second.temperature);
				humidity.push(dataEvery2Second.humidity);
			}
			else {				
				await addDataToDatabase(arrayAvg(celsius), arrayAvg(humidity));
				celsius = [dataEvery2Second.temperature];
				humidity = [dataEvery2Second.humidity];
			}
		}
	})
}, 1000);

/**
 * Upload the params with date and ID to the DB.
 * 
 * @param {num} celsius 
 * @param {num} humidity 
 */
 function addDataToDatabase(celsius, humidity) {
	const dateNow = new Date(Date.now()).getTime();
	
	// load 1min record to DB 
	const newData = new DataModel({
		celsius : celsius,
		humidity: humidity,
		smoke: 0,
		date: dateNow,
		pi: 1
	});

	newData.save((err) => {
		if (err) {
			console.log(err);
		} else {
			console.log("succesful uploading in 1 min.");
		}
	})
}

// Check the server working
router.get("/main", function(req, res){	
	res.send("Működik");
});

// Listing registrated users
router.get('/users', async (req,res) => {
    res.json(await UserModel.find());
})

// Get device's datas
router.get("/oneweek", async (req, res) => {
	const date = new Date(Date.now()).getTime(); 
	const oneDay = 86400000; // in millisec
	const oneWeek = 604800000; // in millisec
	let rowLastWeekHum = [];
	let rowLastWeekCel = [];
	let delIndex = 0;

	for(let i = oneDay; i < oneWeek; i += oneDay) {
		let data = DataModel.find({ date: { $gte:date - i}});
		delIndex = data.length;
		console.log(data);
		let celsiusData = [];
		let humidityData = [];
		
		for(let j = delIndex-1; j >= 0 ; j--) {
			celsiusData = data[j].temperature;
			humidityData = data[j].humidity;
		}
		rowLastWeekCel.push(arrayAvg(celsiusData));
		rowLastWeekHum.push(arrayAvg(humidityData));
	}
	res.send({'celsius' : rowLastWeekCel, 'humidity': rowLastWeekHum});	
});

// Actual temperature and humidity.
let dataOld = {
	humidity: 0,
	temperature: 0
};
router.get("/actualData", async (req, res) => {
	let dataNew;
	fetch(orangePiIpAddress + '/datas')
	.then(async(resp) => {
		dataNew = await resp.json();
		if(dataNew !== null) dataOld = dataNew;
		res.send(dataOld);
	})	
});

// Login endpoint
router.post("/login", async (req, res) => {
	let loginCorrect = {
		correct: false
	};
	const body = req.body;
	console.log(body);
	if (body.email === undefined && body.password) {
		res.send(loginCorrect);
	} else {
		const user = await UserModel.findOne( { email: body.email, password: body.password } );
		if (user) {
			loginCorrect.correct = true;
		}
		res.send(loginCorrect);
	}
});

// Sign up endpoint
router.post("/registration", async (req, res) => {
	let registrationCorrect = {
		correct: false
	};
	
	const body = req.body;
		const userData = {
			name: '' + body.inputName,
			email: '' + body.inputEmail,
			password: '' + body.inputPassword,
			city: '' + body.inputCity,
			zip: '' + body.inputZip,
			address: '' + body.inputAddress,
			phone: '' + body.inputPhone
		};

	const userExists = await UserModel.findOne({ email:userData.email });
	if (userExists) {
		res.send(registrationCorrect)
	} else {
		const newUser = new UserModel({
			name: userData.name,
			email: '' + body.inputEmail,
			password: '' + body.inputPassword,
			city: '' + body.inputCity,
			zip: '' + body.inputZip,
			address: '' + body.inputAddress,
			phone: '' + body.inputPhone
		});
		newUser.save((err) => {
			if (err) {
				res.send(registrationCorrect)
			} else {
				registrationCorrect.correct = true;
				res.send(registrationCorrect)
			}
		})
	}
});


/** 
 * Check the celsius and humidity changing
 * value: summa to array #m element
 * 
 * @param {obj} value 
 * @return: number of the danger level
 */
function hysteresis(value) {
	if (dataFromPi.length < 5) {
		for (let i = 0; i < 5; i++) {
			dataFromPi.push(value);
		}
	} else {
		dataFromPi.shift();
		dataFromPi.push(value);
	}

	// Separate the C and H
	let avgTemperature = [];
	let avgHumidity = [];
	for(let i = 1; i < 5;i++) {
		avgTemperature.push(dataFromPi[i].temperature);
		avgHumidity.push(dataFromPi[i].humidity);
	}

	let firstT = dataFromPi[0].temperature;
	let firstH = dataFromPi[0].humidity;
	let lastT = dataFromPi[4].temperature;
	let lastH = dataFromPi[4].humidity;

	// DANGER check: 
		// temperature change >+-5 
		// humidity change >+-15
	if (Math.abs(firstT - lastT) > 5 &&
		Math.abs(firstT - arrayAvg(avgTemperature)) > 5) {
		if (Math.abs(firstT - lastT) > 5 &&
			Math.abs(firstH - lastH) > 15  &&
			Math.abs(firstT - arrayAvg(avgTemperature)) > 5 &&
			Math.abs(firstH - arrayAvg(avgHumidity)) > 15)
			return 33;
		else return 11;
	} else if (Math.abs(firstH - lastH) > 15 &&
		Math.abs(firstH - arrayAvg(avgHumidity)) > 15) return 22;

	// WARNING check: 
		// temperature change >+-4 && <+-6 
		// humidity change >+-10 && <+-16
	if (Math.abs(firstT - lastT) > 3 &&
		Math.abs(firstT - arrayAvg(avgTemperature)) > 3) {
		if (Math.abs(firstT - lastT) > 3 &&
			Math.abs(firstH - lastH) > 10  &&
			Math.abs(firstT - arrayAvg(avgTemperature)) > 3 &&
			Math.abs(firstH - arrayAvg(avgHumidity)) > 10)
			return 3;
		else return 1;
	} else if (Math.abs(firstH - lastH) > 10 &&
		Math.abs(firstH - arrayAvg(avgHumidity)) > 10) return 2;
	return 0;
};

/** 
 * Average function
 * 
 * @param {num} array avg array
 * @param {num} from sum from array #n element
 * @param {num} to sum to array #m element
 * @return: summa numbers' average
 */
function arrayAvg(array){
	let sum = 0;
	for(let i = 0; i < array.length; i++) {
		sum += array[i];
	}
	return sum/array.length;
}

/**
 * Get the result from the hysteresis() function and 
 * return the correct grammatical answer
 * 
 * @param {num} value: number from hysteresis() function 
 * @return: the correct grammatical answer
 */
function getHysteresisInformation (value) {
	let returnString;
	switch (value) {
		// WARNING!
		case 1:
			returnString = "Figyelem! Hőmérséklet változás!";			
			break;
		case 2:
			returnString = "Figyelem! Páratartalom változás!";
			break;
		case 3:
			returnString = "Figyelem! Hőmérséklet és Páratartalom változás!";
			break;
			
		// DANGER!!!		
		case 1:
			returnString = "Veszély! Hőmérséklet változás!";			
			break;
		case 2:
			returnString = "Veszély! Páratartalom változás!";
			break;
		case 3:
			returnString = "Veszély! Hőmérséklet és Páratartalom változás!";
			break;
			
		default:
			returnString = 0;
			break;
	}
	return returnString;
}

/**
 * Send email when celsius/humidity value has changed.
 * 
 * @param {string} value 
 */
function sendEmail (value) {
	var mailOptions = {
		from: 'example@gmail.com',
		to: 'h646108@stud.u-szeged.hu',
		subject: 'Message from your sensor',
		text: value
	};

	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			console.log(error);
		} else {
			console.log('Email sent: ' + info.response);
		}
	}); 
}

/**
 * Send SMS
 * 
 * @param {string} phone phone number
 * @param {string} value sms string
 */
function sendSMS(phone, value) {
	sms.send(phone, value)
		.then(body => console.log(body))
		.catch(err => console.log(err.message))
}


module.exports = router;